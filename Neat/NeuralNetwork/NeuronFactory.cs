﻿using Neat.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neat.NeuralNetwork
{
    public abstract class AbstractNeuronFactory<T>
    {
        public abstract Neuron<T> GetNeuron(uint ID);
        public abstract Neuron<T> CopySelf(Neuron<T> _neuronToCopy);
        public abstract void Reset(Neuron<T> neuron);
        public abstract void ReturnPool(Neuron<T> neuron);
        public abstract void Drop(Neuron<T> neuron);
    }

    public class NeuronFactory<T> : AbstractNeuronFactory<T>
    {
        Pool<Neuron<T>> _pool = new Pool<Neuron<T>>();

        public override Neuron<T> GetNeuron(uint ID)
        {
            Neuron<T> neuron = _pool.GetObject();
            neuron.UID = ID;
            return neuron;
        }

        public override Neuron<T> CopySelf(Neuron<T> _neuronToCopy)
        {
            /*
             * The new connections will be still to the old neurons.
             * Later on the network will overwrite them.
             */
            Neuron<T> retValue = _pool.GetObject();
            Reset(retValue);

            retValue.UID = _neuronToCopy.UID;
            retValue.isActive = _neuronToCopy.isActive;
            retValue.isInput = _neuronToCopy.isInput;
            retValue.isRecurrent = _neuronToCopy.isRecurrent;

            foreach (Connection<T> elem in _neuronToCopy.Connections)
            {
                retValue.Connections.Add(elem.CopySelf());
            }
            retValue.MapConnections();
            return retValue;
        }

        public override void Reset(Neuron<T> neuron)
        {
            neuron.Connections.Clear();
            neuron.ResetValues();
        }

        public override void ReturnPool(Neuron<T> neuron)
        {
            _pool.GiveObjet(neuron);
        }

        public override void Drop(Neuron<T> neuron)
        {
            Reset(neuron);
            ReturnPool(neuron);
        }
    }
}
