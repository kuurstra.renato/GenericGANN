﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neat.NeuralNetwork
{
    public interface IValue<TType>
    {
        TType value { get; set; }
        TType Add(IValue<TType> x);
        TType Multiply(IValue<TType> x);
        void Init();
        IValue<TType> Squash();
    }

    [System.Serializable]
    public struct ConcreteValue : IValue<float>
    {
        public float value { get; set; }

        public void Init()
        {
            value = 1f;
        }

        public ConcreteValue(float value)
        {
            this.value = value;
        }

        public static ConcreteValue operator +(ConcreteValue a, ConcreteValue b)
        {
            return new ConcreteValue(a.Add(b));
        }
        public static ConcreteValue operator *(ConcreteValue a, ConcreteValue b)
        {
            return new ConcreteValue(a.Multiply(b));
        }

        public float Add(IValue<float> b)
        {
            return this.value + b.value;
        }
        public float Multiply(IValue<float> b)
        {
            return this.value * b.value;
        }
        public IValue<float> Squash()
        {
            return new ConcreteValue((float)Math.Tanh(value));
        }
    }
}
