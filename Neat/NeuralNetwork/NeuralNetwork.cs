﻿using Neat.GeneticAlgorithm;
using Neat.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neat.NeuralNetwork
{

    public interface INeatNetwork<T>
    {
        void Init(NeuronFactory<T> neuronFactory, int numInputsNeurons, int numOutputsNeurons);
        void Update();
        void SetInputs(List<IValue<T>> inputs);
        List<IValue<T>> GetOutputs();
    }

    public abstract class AbstractNetwork<T> : INeatNetwork<T>
    {
        //How on hell we can declare this on a interface?!
        public delegate void NeuronAction(Neuron<T> neuron);

        public abstract void ForEachNeuron(NeuronAction actionOnNeurons);
        public abstract void Init(NeuronFactory<T> neuronFactory, int numInputsNeurons, int numOutputsNeurons);
        public abstract void Update();
        public abstract void SetInputs(List<IValue<T>> inputs);
        public abstract void MapNeurons();
        public abstract List<IValue<T>> GetOutputs();
    }

    public class Network<T> : AbstractNetwork<T>, IGenome<T>
    {
        protected NeuronFactory<T> _neuronFactory;

        /// <summary>
        /// Mapping neurons by ID, so when we look for neurons the speed is O instead of On
        /// Sadly HashSets don't have a way to retrieve the objects having a equivalent one.
        /// </summary>
        public Dictionary<uint, INeatNeuron<T>> neuronMap = new Dictionary<uint, INeatNeuron<T>>();

        // TOTO creating a new class that is specifically suited to contains neurons.
        List<INeatNeuron<T>> _inputNeurons = new List<INeatNeuron<T>>();
        List<INeatNeuron<T>> _outputNeurons = new List<INeatNeuron<T>>();
        HashSet<INeatNeuron<T>> _totalNeuronContainer = new HashSet<INeatNeuron<T>>(new SameNeuronComparer<T>());

        #region NeuronsOperations

        /// <summary>
        /// this is bad but will fix later on
        /// </summary>
        public void ResetNewConnections()
        {
            foreach (var neuron in _totalNeuronContainer)
            {
                var connections = neuron.GetConnections();
                foreach (var item in connections)
                {
                    item.FromNeuron = neuronMap[item.FromNeuron.UID];
                    item.ToNeuron = neuronMap[item.ToNeuron.UID];
                }
            }
        }

        override public void ForEachNeuron(NeuronAction actionOnNeurons)
        {
            //foreach (var item in _inputNeurons)
            //{
            //    actionOnNeurons(item as Neuron<T>);
            //}
            //foreach (var item in _outputNeurons)
            //{
            //    actionOnNeurons(item as Neuron<T>);
            //}
            foreach (var item in _totalNeuronContainer)
            {
                actionOnNeurons(item as Neuron<T>);
            }
        }

        public HashSet<INeatNeuron<T>> GetHiddenLayer()
        {
            return _totalNeuronContainer;
        }

        /// <summary>
        /// Best option is to create tabs of neurons like properties, so we can automatically react to get/sets
        /// </summary>
        override public void MapNeurons()
        {
            ForEachNeuron(AddNeuronTomap);
        }

        void AddNeuronTomap(Neuron<T> neuron)
        {
            if (neuronMap.ContainsKey(neuron.UID) == false)
            {
                neuronMap.Add(neuron.UID, neuron);
            }
        }

        public void SetInputNeurons(List<INeatNeuron<T>> neuronList)
        {
            _inputNeurons = neuronList;
            MapNeurons();
        }

        public void SetOutputNeurons(List<INeatNeuron<T>> neuronList)
        {
            _outputNeurons = neuronList;
            MapNeurons();
        }

        public void SetNeuronContainer(HashSet<INeatNeuron<T>> neuronHashset)
        {
            _totalNeuronContainer = neuronHashset;
            MapNeurons(); // we map the new neurons
            ResetNewConnections(); // we overwrite each neuron inside each connection
        }

        public List<INeatNeuron<T>> GetInputNeurons()
        {
            return _inputNeurons;
        }
        public List<INeatNeuron<T>> GetOutputNeurons()
        {
            return _outputNeurons;
        }

        public HashSet<INeatNeuron<T>> GetHiddenNeurons()
        {
            return _totalNeuronContainer;
        }

        #endregion

        #region GenomeMethods

        List<IValue<T>> c_outputs = new List<IValue<T>>();
        public List<IValue<T>> ExecuteGenome(List<IValue<T>> inputs)
        {
            for (int i = 0; i < inputs.Count; i++)
            {
                _inputNeurons[i].SetValue(inputs[i]);
            }
            Update();
            c_outputs.Clear();
            for (int i = 0; i < _outputNeurons.Count; i++)
            {
                c_outputs.Add(_outputNeurons[i].GetValue());
            }
            return c_outputs;
        }

        #endregion

        public IEnumerable<INeatNeuron<T>> CopyNeurons(IEnumerable<INeatNeuron<T>> neuronList)
        {
            List<INeatNeuron<T>> c_retValueCopyNeurons = new List<INeatNeuron<T>>();
            foreach (Neuron<T> elem in neuronList)
            {
                c_retValueCopyNeurons.Add(_neuronFactory.CopySelf(elem));
            }
            return c_retValueCopyNeurons;
        }

        /// <summary>
        /// Initialize the network. It will also create the containers needed by the network
        /// currently the "all neurons" has only input and hidden layer, not the output because they will always have 0 connections
        /// </summary>
        /// <param name="neuronFactory"></param>
        /// <param name="numInputsNeurons"></param>
        /// <param name="numOutputsNeurons"></param>
        public override void Init(NeuronFactory<T> neuronFactory, int numInputsNeurons, int numOutputsNeurons)
        {
            _neuronFactory = neuronFactory;
            if (numInputsNeurons == _inputNeurons.Count && numOutputsNeurons == _outputNeurons.Count)
            {
                return;
            }
            //Carefull, if in the future the size will change we need to return the neurons to the factory pool
            _inputNeurons.Clear();
            for (int i = 0; i < numInputsNeurons; i++)
            {
                _inputNeurons.Add(_neuronFactory.GetNeuron(UniqueId<NeuronFactory<T>>.Instance.GetNewID()));
            }
            _outputNeurons.Clear();
            for (int i = 0; i < numOutputsNeurons; i++)
            {
                _outputNeurons.Add(_neuronFactory.GetNeuron(UniqueId<NeuronFactory<T>>.Instance.GetNewID()));
            }
            _totalNeuronContainer.Clear();
            _totalNeuronContainer.UnionWith(_inputNeurons);
            //_totalNeuronContainer.UnionWith(_outputNeurons);
            MapNeurons();
        }

        public override void SetInputs(List<IValue<T>> inputs)
        {
            for (int i = 0; i < inputs.Count; i++)
            {
                _inputNeurons[i].SetValue(inputs[i]);
            }
        }

        List<IValue<T>> Outputs = new List<IValue<T>>();
        public override List<IValue<T>> GetOutputs()
        {
            Outputs.Clear();
            for (int i = 0; i < _outputNeurons.Count; i++)
            {
                Outputs.Add(_outputNeurons[i].GetValue());
            }
            return Outputs;
            //return OutputNeurons;
        }

        public void ConnectAllInputsOutputs(IValue<T> weight)
        {
            foreach (var inNeuron in _inputNeurons)
            {
                foreach (var outNeuron in _outputNeurons)
                {
                    inNeuron.AddConnection(outNeuron, weight, UniqueId<Connection<T>>.Instance.GetNewID());
                }
            }
        }

        Queue<INeatNeuron<T>> neuronsToUpdate = new Queue<INeatNeuron<T>>();
        HashSet<Connection<T>> connections;
        public override void Update()
        {
            neuronsToUpdate.Clear();

            foreach (Neuron<T> elem in _inputNeurons)
            {
                neuronsToUpdate.Enqueue(elem);
            }

            while (neuronsToUpdate.Count != 0)
            {
                Neuron<T> updateNeuron = neuronsToUpdate.Dequeue() as Neuron<T>;
                if (updateNeuron.isUpdated == true)
                {
                    continue;
                }
                else
                {
                    connections = updateNeuron.GetConnections();
                    foreach (var item in connections)
                    {
                        neuronsToUpdate.Enqueue(item.ToNeuron);
                    }

                    updateNeuron.Update();
                    updateNeuron.isUpdated = true;
                }
            }
        }

    }
}
