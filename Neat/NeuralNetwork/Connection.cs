﻿using Neat.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neat.NeuralNetwork
{

    public interface IGANNConnection<T>
    {
        uint ID { get; set; }
        INeatNeuron<T> ToNeuron { get; set; }
        INeatNeuron<T> FromNeuron { get; set; }
        IValue<T> GetWeight();
        void Setup(INeatNeuron<T> from, INeatNeuron<T> to, IValue<T> weight, uint ID);
    }

    public class SameConnectionComparer<T> : EqualityComparer<Connection<T>>
    {
        public override bool Equals(Connection<T> c1, Connection<T> c2)
        {
            return c1.ID == c2.ID || (c1.FromNeuron == c2.FromNeuron && c1.ToNeuron == c2.ToNeuron);
        }

        public override int GetHashCode(Connection<T> neuron)
        {
            return (int)unchecked(neuron.ID);
        }
    }

    [System.Serializable]
    public class Connection<T> : IDeepCopy<Connection<T>>, IGANNConnection<T>
    {
        public Connection<T> CopySelf()
        {
            Connection<T> retValue = new Connection<T>();

            retValue.ID = this.ID;
            retValue.ToNeuron = this.ToNeuron;
            retValue.Weight = this.Weight;
            retValue.FromNeuron = this.FromNeuron;

            return retValue;
        }

        public void Setup(INeatNeuron<T> fromNeuron, INeatNeuron<T> toNeuron, IValue<T> weight, uint ID)
        {
            this.FromNeuron = fromNeuron;
            this.ToNeuron = toNeuron;
            this.Weight = weight;
            this.ID = ID;
        }

        public IValue<T> GetWeight()
        {
            return Weight;
        }

        public void SetWeight(IValue<T> weight)
        {
            Weight = weight;
        }

        public INeatNeuron<T> ToNeuron { get; set; }
        public INeatNeuron<T> FromNeuron { get; set; }
        public IValue<T> Weight;
        public uint ID { get; set; }
    }
}
