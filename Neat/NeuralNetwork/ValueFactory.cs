
namespace Neat.NeuralNetwork
{
    public class FloatValueFactory
    {
        public IValue<float> GetNewValue(float value)
        {
            return new ConcreteValue(value);
        }
    }
}