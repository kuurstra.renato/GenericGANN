﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neat.NeuralNetwork
{
    public interface INeatNeuron<T>
    {
        IValue<T> Update();
        uint UID { get; set; }
        void AddConnection(INeatNeuron<T> other, IValue<T> weight, uint ID);
        void RemoveConnection(uint ID);
        void SetValue(IValue<T> value);
        HashSet<Connection<T>> GetConnections();
        IValue<T> GetValue();
    }

    class SameNeuronComparer<T> : EqualityComparer<INeatNeuron<T>>
    {
        public override bool Equals(INeatNeuron<T> n1, INeatNeuron<T> n2)
        {
            return n1.UID == n2.UID;
        }

        public override int GetHashCode(INeatNeuron<T> neuron)
        {
            return (int)unchecked(neuron.UID);
        }
    }

    public abstract class AbstractNeuron<T> : INeatNeuron<T>
    {
        public abstract IValue<T> Update();
        public abstract uint UID { get; set; }
        public abstract void AddConnection(INeatNeuron<T> other, IValue<T> weight, uint ID);
        public abstract void RemoveConnection(uint ID);
        public abstract void SetValue(IValue<T> value);
        public abstract IValue<T> GetValue();
        public abstract HashSet<Connection<T>> GetConnections();
    }

    public class Neuron<T> : AbstractNeuron<T>
    {
        public override uint UID { get; set; }
        public bool isActive;
        public bool isInput;
        public bool isUpdated;
        public bool isRecurrent;

        protected IValue<T> _value;
        protected IValue<T> accValue;

        public void ResetValues()
        {
            _value = default(IValue<T>);
            accValue = default(IValue<T>);
            UID = 0;
        }

        public Dictionary<uint, Connection<T>> ConnectionsMap = new Dictionary<uint, Connection<T>>();
        public HashSet<Connection<T>> Connections = new HashSet<Connection<T>>(new SameConnectionComparer<T>());

        //Todo Optimize
        public void MapConnection(Connection<T> conn)
        {
            ConnectionsMap.Add(conn.ID, conn);
        }
        public void MapConnections()
        {
            foreach (var item in Connections)
            {
                if(ConnectionsMap.ContainsKey(item.ID) == false)
                {
                    ConnectionsMap.Add(item.ID, item);
                }
            }
        }

        override public HashSet<Connection<T>> GetConnections()
        {
            return Connections;
        }

        override public IValue<T> Update()
        {
            return _value;
        }

        override public void SetValue(IValue<T> value)
        {
            _value = value;
        }

        override public IValue<T> GetValue()
        {
            return _value;
        }

        public override void AddConnection(INeatNeuron<T> other, IValue<T> weight, uint ID)
        {
            Connection<T> conn = new Connection<T>();

            conn.Setup(this, other, weight, ID);

            if (Connections.Add(conn) == true)
            {
                Console.WriteLine("Add connection from " + this.UID + " to " + other.UID );
                MapConnection(conn);
            }
            else
            {
                Console.WriteLine("Skip connection from " + this.UID + " to " + other.UID + " same ID/Connection ");
            }

        }

        // should probably check if a connection already exists between these neurons.
        public static void AddConnection(AbstractNeuron<T> fromNeuron,
                                         AbstractNeuron<T> toNeuron,
                                         IValue<T> weight,
                                         uint ID)
        {
            fromNeuron.AddConnection(toNeuron, weight, ID);
        }


        // LOL C# don't have a remove[] for a hashset LOL LOL
        public override void RemoveConnection(uint ID)
        {
            Connections.RemoveWhere((Connection<T> con) =>
            {
                if (con.ID == ID)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            });
        }

        public static void RemoveConnection(AbstractNeuron<T> fromNeuron, uint ID)
        {
            fromNeuron.RemoveConnection(ID);
        }

    }
}
