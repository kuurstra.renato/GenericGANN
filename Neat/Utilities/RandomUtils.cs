using System;

namespace Neat.Utilities
{
    class Rng : Random
    {
        public Rng(int seed) : base(seed)
        {
        }

        // return a float between -1 and 1
        public float MOnePOne()
        {
            return (float) ( Sample() * 2f ) - 1f;
        }

        public float InRange(float min, float max)
        {
            return (float) ( (Sample() * (max - min) ) + min);
        }
        
    }
}