﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neat.Utilities
{
    class UniqueId<T>
    {
        uint _currentMaxId = 0;
        Queue<uint> _idNotUsed = new Queue<uint>();

        private static UniqueId<T> instance;

        private UniqueId() { }

        public static UniqueId<T> Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UniqueId<T>();
                }
                return instance;
            }
        }

        public uint GetNewID()
        {
            uint retvalue;
            //Console.WriteLine(this.GetType().ToString() + " current ID: " + _currentMaxId);
            if (_idNotUsed.Count == 0)
            {
                retvalue = _currentMaxId;
                _currentMaxId++;
                return retvalue;
            }
            else
            {
                return _idNotUsed.Dequeue();
            }
        }

        public void ReturnID(uint retId)
        {
            _idNotUsed.Enqueue(retId);
        }
    }
}
