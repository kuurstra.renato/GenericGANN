﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neat.Utilities
{
    public interface IPooled
    {
        /// <summary>
        /// We need a method to reset the state of the object 
        /// </summary>
        void Reset();
        /// <summary>
        /// returns the object to the appropriate pool
        /// </summary>
        void ReturnPool();
        /// <summary>
        /// Reset+return to pool
        /// </summary>
        void Drop();
    }

    public interface IDeepCopy<T>
    {
        T CopySelf();
    }
}
