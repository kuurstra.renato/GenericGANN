﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neat.Utilities
{
    /// <summary>
    /// Pool system. Need to have parameter less constructor
    /// </summary>
    [System.Serializable]
    public class Pool<T> where T : new()
    {
        List<T> _pool;

        public Pool()
        {
            _pool = new List<T>();
        }

        public T GetObject()
        {
            if (_pool.Count == 0)
            {
                return Create();
            }
            else
            {
                T ret_value;
                ret_value = _pool[_pool.Count - 1];
                _pool.RemoveAt(_pool.Count - 1);
                return ret_value;
            }
        }

        public T Create()
        {
            return new T();
        }

        public void GiveObjet(T obj)
        {
            _pool.Add((obj));
        }
    }
}
