﻿using Neat.NeuralNetwork;
using Neat.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neat.GeneticAlgorithm
{
    interface IEvolutionaryPool<T>
    {
        List<ISolution<T>> population { get; set; }
    }

    /// <summary>
    /// This is the main class to create and deal with the pool of solutions.
    /// Main concepts:
    /// 1) Specie: Each new solution will be evaluated against the specie table. If the distance ( a mix of hamming and cosine distnace)
    ///            is high enough from all existing species a new one will be created.
    /// 2) Matriarch: Each specie will have a matriarch. This solution is special. First its used to calculate the distance from the
    ///               species, as a "role model". Second its for all intend and purpose a Elite solution. Even when a solution is killed
    ///               the matriarch will be a valid cross breed candidate. Each time the fitness to the pool is changed we need to evaluate
    ///               the specie table to "elect" a possible new matriarch.
    /// 3) Discretization time: This GA work in real time, by Discretization of time. Instead of generations, we have time. this allow various us:
    ///         a - We evaluate a solution in a continous time enviroment. We can calculate a fitness / time ratio, and kill unproductive genes at altered time.
    ///         b - The load on the machine will be much more distribuited, when we train a GA pool a network on a set of data we kill/generate solutions
    ///             in a more costant rate. 
    ///         c - We can evaluate solutions based on age, applying different algorithms to deal with different problems.
    ///         d - There are many statistics that allow us to leverage time in this ambient. (*too tired to write more stuff, but there is:P)
    ///               
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EvolutionaryPool<T> : IEvolutionaryPool<T>
    {
        public class EvolutionParams
        {
            public float NewSpeciesDistance = 0.2f;
        }

        public class EvolutionaryPoolStats
        {
            public float totalFitness;
        }

        Random random = new Random();

        Rng _rng = new Rng(0);

        private SolutionFactory<T> _solutionFactory;
        private EvolutionParams _evolutionParams;
        EvolutionaryPoolStats EvolutionStats;

        //Fitness sharing
        //Solution Factory
        /// <summary>
        /// Should try to make it a sequential instead of a dictionary for faster approach,
        /// but we also need the random access of a dictionary 
        /// </summary>
        public List<ISolution<T>> population { get; set; }

        public Dictionary<uint, ISolution<T>> populationIndexed = new Dictionary<uint, ISolution<T>>();


        /// Each key is unique uint key that rapresent a new species.
        /// A new species is created after creating a new solution, if the new solution
        /// is distant enough from the Matriarch of the species ( the solution that initiated the species), and is distant
        /// enough from all other species.
        /// A specie in also used like a island, they can't reproduce with "outsider" unless very "lucky". Every now and then
        /// the least performant species will be removed.
        /// As of now the individuals dictionary is just used for selection wheel
        public class Specie
        {
            public ISolution<T> Matriarch;
            public float maxFitness;
            public float totalFitness;
            public List<ISolution<T>> individuals = new List<ISolution<T>>();
        }
        Dictionary<uint, Specie> _speciesTable = new Dictionary<uint, Specie>();
        //information we may need is how much fitness each species has.

        public EvolutionaryPool(int MaxPopulation, EvolutionParams evolutionParams, SolutionFactory<T> solutionFactory)
        {
            _solutionFactory = solutionFactory;
            _evolutionParams = evolutionParams;
            population = new List<ISolution<T>>();
            EvolutionStats = new EvolutionaryPoolStats();

            ISolution<T> solution = solutionFactory.GetNewSolution();
            solution.Randomize();
            AddSolutionToPool(solution);
            // We already created one solution
            for (int i = 0; i < MaxPopulation - 1; i++)
            {
                ISolution<T> offSpring = _solutionFactory.CopySolution(solution);
                offSpring.Randomize();
                AddSolutionToPool(offSpring);
            }

        }

        /// <summary>
        /// Add solution to pool:
        /// 1) Add to population and population Indexed
        /// 2) Check if the Hamming distance is low enough vs Matriarch of all the species
        /// 3) Create a new specie if needed, or add it to a existing one
        /// </summary>
        /// <param name="solution"></param>
        void AddSolutionToPool(ISolution<T> solution)
        {
            population.Add(solution);
            populationIndexed.Add(solution.UID, solution);

            uint? specieUID = null;
            foreach (var specie in _speciesTable)
            {

                float distance = (solution.DistanceFrom(specie.Value.Matriarch));
                //Console.WriteLine("distance: " + distance.ToString());

                if (distance < _evolutionParams.NewSpeciesDistance)
                {
                    specieUID = specie.Key;
                    break;
                }
            }
            if (specieUID == null)
            {
                NewSpecie(solution);
            }
            else
            {
                _speciesTable[(uint)specieUID].individuals.Add(solution);
            }
        }

        /// <summary>
        /// TODO:
        /// Remove last element from list, make it multithread.
        /// Remove from population and indexed. Also from specie, and extinguish a specie if it was the last element of it
        /// </summary>
        /// <param name="solution"></param>
        void RemoveSolutionFromPool(ISolution<T> solution)
        {
            population.Remove(solution);
            populationIndexed.Remove(solution.UID);
            _speciesTable[solution.Specie].individuals.Remove(solution);
            if (_speciesTable[solution.Specie].individuals.Count == 0)
            {
                _speciesTable.Remove(solution.Specie);
            }
        }


        #region Species
        /// <summary>
        /// A new specie is needed, so we add the matriarch to it. Later on it will be the fittest solution for the specie
        /// </summary>
        /// <param name="_matriarch"></param>
        public void NewSpecie(ISolution<T> matriarch)
        {
            Specie newSpecie = new Specie();
            newSpecie.Matriarch = matriarch;
            newSpecie.individuals.Add(matriarch);
            matriarch.Specie = UniqueId<Specie>.Instance.GetNewID();
            _speciesTable.Add(matriarch.Specie, newSpecie);
            Console.WriteLine("New specie created id: " + matriarch.Specie);
        }
        #endregion


        /// <summary>
        /// Update all the solutions in the population
        /// </summary>
        /// <param name="inputs"> The external list is for the population individual, the inner one for the inputs</param>
        /// <param name="deltaTime"> how much time has passed since last evalutation </param>
        /// <returns> A list of list of outputs </returns>
        List<List<IValue<T>>> c_executeAllSolutions_retvalue = new List<List<IValue<T>>>();
        public List<List<IValue<T>>> ExecuteAllSolutions(List<List<IValue<T>>> inputs, float deltaTime)
        {
            c_executeAllSolutions_retvalue.Clear();
            for (int i = 0; i < population.Count; i++)
            {
                c_executeAllSolutions_retvalue[i].AddRange(population[i].ExecuteSolution(inputs[i]));
                population[i].Age += deltaTime;
            }
            return c_executeAllSolutions_retvalue;
        }

        /// <summary>
        /// Add fitness to all solutions
        /// </summary>
        /// <param name="fitness"> for each solution in table, we </param>
        public void AddFitnessAllSolutions(List<float> fitness)
        {
            for (int i = 0; i < population.Count; i++)
            {
                population[i].Fitness += fitness[i];
            }
            // When we change the fitness we need to recalculate the single species.
            CalculateFitnessStatsSpecies();
        }

        /// <summary>
        /// Change the fitness states inside the various species
        /// Switch matriarchs if a new individual is fitter
        /// </summary>
        public void CalculateFitnessStatsSpecies()
        {
            float totalFitness = 0.0f;
            foreach (var specieKeyValuePair in _speciesTable)
            {
                ISolution<T> newMatriarch = null;
                float newMatriarchFitness = 0f;
                Specie specie = specieKeyValuePair.Value;
                specie.totalFitness = 0f;
                specie.maxFitness = 0f;

                foreach (var item in specie.individuals)
                {
                    if (item.Fitness > newMatriarchFitness)
                    {
                        newMatriarchFitness = specie.maxFitness = item.Fitness;
                        newMatriarch = item;
                    }
                    specie.totalFitness += item.Fitness;
                }
                totalFitness += specie.totalFitness;
                specie.Matriarch = newMatriarch;
            }
            EvolutionStats.totalFitness = totalFitness;
        }

        /// <summary>
        /// Kill solution at index, and do nothing else. If remove is true it means
        /// that a new solution will not be created, so the population will shrink.
        /// So the population will decrease if another solution isnt created.
        /// </summary>
        /// <param name="populationIndex"></param>
        /// <param name="remove"></param>
        public void KillSolution(int populationIndex, bool remove = false)
        {
            // Check if is a Matriarch, if so we add to a queue
            // Remove from species table
            // reset it or if remove is true remove from table enterly
            // generate a new solution
        }

        /// <summary>
        /// Create a new solution.
        /// This will increase the population if another solution isn't killed.
        /// </summary>
        public void NewSolution()
        {

            // Choose both parents
            KeyValuePair<ISolution<T>, ISolution<T>> parents = SelectCandidates();

            // Breed them inside a new solution

            // Mutate solution
            // Evaluate for speciation
            // insert in Species table and population table
        }

        /// <summary>
        /// Double roulette that returns two solutions to be breeded
        /// </summary>
        /// <returns></returns>
        public KeyValuePair<ISolution<T>, ISolution<T>> SelectCandidates()
        {
            float fitnessToPass = _rng.InRange(0f, EvolutionStats.totalFitness);

            uint FirstParent = RouletteSelect(fitnessToPass);
            fitnessToPass += EvolutionStats.totalFitness / 2f;
            fitnessToPass = (fitnessToPass > EvolutionStats.totalFitness) ? fitnessToPass - EvolutionStats.totalFitness : fitnessToPass;
            uint SecondParent = RouletteSelect(fitnessToPass);
            KeyValuePair<ISolution<T>, ISolution<T>> candidates = new KeyValuePair<ISolution<T>, ISolution<T>>(populationIndexed[FirstParent], populationIndexed[SecondParent]);

            return candidates;
        }

        uint RouletteSelect(float fitnessToPass)
        {
            float fitnessPassed = 0f;
            foreach (var solution in population)
            {
                fitnessPassed += solution.Fitness;
                if (fitnessPassed >= fitnessToPass)
                {
                    return solution.UID;
                }
            }
            //Most probably if we reach here is because total fitness is 0 OR rounding error of floats
            return population.Last().UID;
        }

        ISolution<T> BreedSolutions(KeyValuePair<ISolution<T>, ISolution<T>> parents)
        {
            return parents.Key.CrossBreed(parents.Value);
        }
    }
}