﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Neat.GeneticAlgorithm.Strategies;
using Neat.NeuralNetwork;
using Neat.Utilities;

namespace Neat.GeneticAlgorithm
{
    public abstract class AbstractSolutionFactory<T>
    {
        protected Queue<ISolution<T>> _poolSolutions = new Queue<ISolution<T>>();
        protected Queue<IGenome<T>> _poolGenomes = new Queue<IGenome<T>>();
        public abstract ISolution<T> GetNewSolution();
        public abstract ISolution<T> GetSolution();
        public abstract void ReturnSolution(ISolution<T> solution);
        public abstract ISolution<T> CopySolution(ISolution<T> solution);
        //public abstract void BuildSolution(IEvolutionStrategy<T> strategy, ISolution<T> solution);
    }

    public class SolutionFactory<T> : AbstractSolutionFactory<T>
    {
        IEvolutionStrategy<T> _evolutionStrategy;
        AbstractGenomeFactory<T> _genomeFactory;

        public SolutionFactory(IEvolutionStrategy<T> evolutionStrategy, AbstractGenomeFactory<T> genomeFactory)
        {
            this._evolutionStrategy = evolutionStrategy;
            this._genomeFactory = genomeFactory;
        }

        override public ISolution<T> GetNewSolution()
        {
            IGenome<T> genome = _genomeFactory.GetNewGenome();
            ISolution<T> solution = new Solution<T>();
            solution.Build(_evolutionStrategy, genome);
            solution.Init();
            solution.UID = UniqueId<ISolution<T>>.Instance.GetNewID();
            return solution;
        }

        //Weirdest thing, if genome is inside build it gets to null
        override public ISolution<T> GetSolution()
        {
            IGenome<T> genome = _genomeFactory.GetGenome();
            ISolution<T> solution = _poolSolutions.Count == 0 ? new Solution<T>() : _poolSolutions.Dequeue();
            solution.Build(_evolutionStrategy, genome);
            solution.UID = UniqueId<ISolution<T>>.Instance.GetNewID();
            return solution;
        }

        override public void ReturnSolution(ISolution<T> solution)
        {
            _genomeFactory.ReturnGenome(solution.genome);
            UniqueId<ISolution<T>>.Instance.ReturnID(solution.UID);
            solution.Reset();
            _poolSolutions.Enqueue(solution);
        }

        override public ISolution<T> CopySolution(ISolution<T> solution)
        {
            ISolution<T> ret_value = GetSolution();
            ret_value.Build(_evolutionStrategy, _genomeFactory.CopyGenome(solution.genome));
            return ret_value; 
        }

    }
}
