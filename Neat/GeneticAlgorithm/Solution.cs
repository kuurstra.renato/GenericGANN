﻿using Neat.GeneticAlgorithm.Strategies;
using Neat.NeuralNetwork;
using Neat.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neat.GeneticAlgorithm
{
    /// <summary>
    /// Interface needed by the Genetic Pool. The concrete object will need to have a strategy set to deal
    /// with the logic, and a solution of type TSolutionType, which will be the implementation of the object
    /// we will work on in the GA
    /// </summary>
    /// <typeparam name="TSolutionType"> Interface used by the object worked by the GA </typeparam>
    public interface ISolution<T>
    {
        uint UID { get; set; }
        uint Specie { get; set; }
        float Age { get; set; }
        float Fitness { get; set; }
        uint Complexity { get; set; }
        IGenome<T> genome { get; }
        float ParentAvrgFitness { get; set; }

        void Reset();
        void Build(IEvolutionStrategy<T> evolutionStrategy, IGenome<T> genome);
        void Randomize();
        void Mutate();
        void Init();
        ISolution<T> CrossBreed(ISolution<T> otherSolution);
        List<IValue<T>> ExecuteSolution(List<IValue<T>> input);
        float DistanceFrom( ISolution<T> otherSolution);
    }

    /// <summary>
    /// A solution can be anything, as long as it can be feed inputs, and after exection returns some outputs
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IGenome<T>
    {
        List<IValue<T>> ExecuteGenome(List<IValue<T>> inputs);
    }

    /// <summary>
    /// Generic implementation of a solution, used by Genetic Pool.
    /// </summary>
    /// <typeparam name="TSolutionType"></typeparam>
    /// <typeparam name="T"></typeparam>
    public class Solution<T> : ISolution<T> 
    {
        public void Reset()
        {
            UniqueId<ISolution<T>>.Instance.ReturnID(UID);
            this.Age = 0f;
            this.Fitness = 0f;
            _evolutionStrategy = null;
            genome = null;
            Complexity = 0;
        }

        public uint UID { get; set; }
        public uint Specie { get; set; }
        public float Age { get; set; }
        public float Fitness { get; set; }
        public uint Complexity { get; set; }
        public IGenome<T> genome { get; private set; }
        public float ParentAvrgFitness { get; set; }

        IEvolutionStrategy<T> _evolutionStrategy;

        public void Build(IEvolutionStrategy<T> evolutionStrategy, IGenome<T> genome)
        {
            this._evolutionStrategy = evolutionStrategy;
            this.genome = genome;
        }

        public void Init()
        {
            this._evolutionStrategy.InitGenome(genome);
        }

        public void Randomize()
        {
            _evolutionStrategy.RandomizeSolution( genome );
        }

        public void Mutate()
        {
            _evolutionStrategy.Mutate(genome);
        }

        public ISolution<T> CrossBreed(ISolution<T> otherSolution)
        {
            return _evolutionStrategy.CrossBreed(this, otherSolution);
        }

        public List<IValue<T>> ExecuteSolution(List<IValue<T>> inputs)
        {
            return genome.ExecuteGenome(inputs);
        }

        public float DistanceFrom(ISolution<T> otherSolution)
        {
            return _evolutionStrategy.Distance(this.genome, otherSolution.genome);
        }
    }
}
