﻿using Neat.NeuralNetwork;
using System;
using System.Collections.Generic;

namespace Neat.GeneticAlgorithm
{
    public abstract class AbstractGenomeFactory<T>
    {
        protected Queue<IGenome<T>> _pool = new Queue<IGenome<T>>();
        public abstract IGenome<T> GetGenome();
        public abstract IGenome<T> GetNewGenome();
        public abstract void ReturnGenome(IGenome<T> genome);
        public abstract IGenome<T> CopyGenome(IGenome<T> genome);
    }

    public class GenomeFactory<T> : AbstractGenomeFactory<T>
    {
        int _inputNeurons;
        int _outputNeurons;
        NeuronFactory<T> _neuronFactory;

        public GenomeFactory(int inputs, int outputs, NeuronFactory<T> neuronFactory)
        {
            _inputNeurons = inputs;
            _outputNeurons = outputs;
            _neuronFactory = neuronFactory;
        }

        override public IGenome<T> GetGenome()
        {
            Network<T> ret_value = _pool.Count == 0 ? new Network<T>() : _pool.Dequeue() as Network<T>;
            return ret_value;
        }

        override public IGenome<T> GetNewGenome()
        {
            Network<T> ret_value = new Network<T>();
            ret_value.Init(_neuronFactory, _inputNeurons, _outputNeurons);
            return ret_value;
        }

        override public void ReturnGenome(IGenome<T> genome)
        {
            _pool.Enqueue(genome);
        }

        //TODO refactor this shit.
        override public IGenome<T> CopyGenome(IGenome<T> genome)
        {
            Network<T> network = genome as Network<T>;
            Network<T> ret_value = GetGenome() as Network<T>;
            if (network == null || ret_value == null) Console.WriteLine("Error, genome is not network");
            // TODO: Change copyNeurons to this factory
            ret_value.SetInputNeurons( network.CopyNeurons(network.GetInputNeurons()) as List<INeatNeuron<T>> );
            ret_value.SetOutputNeurons( network.CopyNeurons(network.GetOutputNeurons()) as List<INeatNeuron<T>> );
            IEnumerable<INeatNeuron<T>> hiddenNeurons = network.CopyNeurons(network.GetHiddenNeurons() as HashSet<INeatNeuron<T>>);
            HashSet<INeatNeuron<T>> neuronTotals = new HashSet<INeatNeuron<T>>(hiddenNeurons, new SameNeuronComparer<T>());
            neuronTotals.UnionWith(ret_value.GetInputNeurons());
            neuronTotals.UnionWith(ret_value.GetOutputNeurons());
            
            ret_value.SetNeuronContainer(neuronTotals);
            //Generate maps
            ret_value.MapNeurons();
            return ret_value;
        }
    }
}
