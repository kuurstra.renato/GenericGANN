﻿using Neat.NeuralNetwork;
using Neat.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
///using System.Linq;

namespace Neat.GeneticAlgorithm.Strategies
{
    public class StrategyParams
    {
        public int inputNeurons = 10;
        public int outputNeurons = 10;

        public float distanceNeuronMult = 0.5f;
        public float distanceConnectionMult = 0.3f;
        public float distanceAvrgWeight = 1.0f;

        /// <summary>
        /// The chance for a weight to get a totally random value.
        /// </summary>
        public float mutationRate = 0.01f;

        /// <summary>
        /// This is how much the weights will change every time a new solution is created.
        /// Each weight WILL change, even so slightly.
        /// </summary>
        public float noiseRate = 1f;

        /// <summary>
        /// Chances that a new neuron will be spawned during mutation
        /// This will change depending on the density of the graph ( network )
        /// </summary>
        public float addNeuron = 0.01f;

        // Disable neuron
        public float disableNeuron = 0.011f;

        // Add Connection
        public float addConnection = 0.02f;

        // Disable Connection
        public float disableConnection = 0.022f;

    }


    /// <summary>
    /// This strategy will deal with anything that is related to the evolution of the solution,
    /// providing the solution with hooks for the genetic pool to operate in a isolate way
    /// CrossBreed
    /// mutate
    /// </summary>
    /// <typeparam name="TSolutionType"></typeparam>
    /// <typeparam name="T"></typeparam>
    public interface IEvolutionStrategy<T>
    {
        void InitGenome(IGenome<T> solution);
        void Mutate(IGenome<T> solution);
        ISolution<T> CrossBreed(ISolution<T> solution, ISolution<T> otherSolution);
        void RandomizeSolution(IGenome<T> solution);
        float Distance(IGenome<T> solutionA, IGenome<T> solutionB);
    }

    abstract public class Strategy<T> : IEvolutionStrategy<T>
    {
        abstract public void InitGenome(IGenome<T> genome);
        abstract public void Mutate(IGenome<T> genome);
        abstract public ISolution<T> CrossBreed(ISolution<T> solution, ISolution<T> otherSolution);
        abstract public void RandomizeSolution(IGenome<T> genome);
        abstract public float Distance(IGenome<T> genomeA, IGenome<T> genomeB);
    }



    public class NeatStrategy<T> : Strategy<T>
    {
        FloatValueFactory _valueFactory = new FloatValueFactory();
        Rng _rng = new Rng(3);
        StrategyParams _strategyParams;
        NeuronFactory<T> _neuronFactory;
        IValue<T> _neutralWeight;

        public NeatStrategy(StrategyParams strategyParams, IValue<T> neutralWeight, NeuronFactory<T> neuronFactory)
        {
            _neutralWeight = neutralWeight;
            _strategyParams = strategyParams;
            _neuronFactory = neuronFactory;
        }

        // Neat strategy assume the network is of type Network<T>
        override public void InitGenome(IGenome<T> solution)
        {
            Network<T> network = solution as Network<T>;
            network.Init(_neuronFactory, _strategyParams.inputNeurons, _strategyParams.outputNeurons);
            network.ConnectAllInputsOutputs(_neutralWeight);
            network.MapNeurons();
        }
        override public void Mutate(IGenome<T> solution)
        {
            /* check if the network should shrink or increase ( or stay the same )
                    If Shrink disable a neuron ( connections get weight 1 )
            
                    if we have a increase we check current connections vs total max connections
                            if < of  param: SparseLimit  we add connection
                            else we add a neuron
                       
            */
        }

        override public void RandomizeSolution(IGenome<T> solution)
        {
            Network<T> network = solution as Network<T>;
            network.ForEachNeuron(RandomizeNeuron);
        }

        void RandomizeNeuron(Neuron<T> neuron)
        {
            foreach (var item in neuron.Connections)
            {
                item.SetWeight(_valueFactory.GetNewValue(_rng.MOnePOne()) as IValue<T>);
            }
        }

        /*
            TODO:
            Random classes with control on seed.
            Delegate that automatically work on all connections inside the network/neuron
            Random mutate should have a multiplier, as a parameter. This will be changed according to:
                1) Steepness of the total specie fitness increase( or decrease ) (flat line will increase, steep will decrease etc )
                2) Distance of the parents from the "patriarch" The futher away they are the stronger will be the mutation.
         */

        /// <summary>
        /// Hamming Distance. Calculated as follow:
        /// 1) Different neurons / total neuron in two networks
        /// 2) Different connections / total connections for each neuron with same ID
        /// 3) For each neuron with same ID we make a averange weight difference of their similar connections
        /// 4) each of these value get multiply by a weight in parameters
        /// </summary>
        /// <param name="solutionA"></param>
        /// <param name="solutionB"></param>
        /// <returns></returns>
        override public float Distance(IGenome<T> solutionA, IGenome<T> solutionB)
        {
            Network<T> networkA = solutionA as Network<T>;
            Network<T> networkB = solutionB as Network<T>;

            HashSet<INeatNeuron<T>> hiddenLayerA = networkA.GetHiddenLayer();
            HashSet<INeatNeuron<T>> hiddenLayerB = networkB.GetHiddenLayer();

            HashSet<INeatNeuron<T>> CommonNeurons = networkA.GetHiddenLayer();
            CommonNeurons.IntersectWith(hiddenLayerB);
            // The sum of the count of the two hidden layers neurons is the total of the neurons.
            // if all of the neuron in the first are present in the second distance is 0 and so the symmetricexept count.
            int totalExcections = (hiddenLayerA.Count - CommonNeurons.Count) + (hiddenLayerB.Count - CommonNeurons.Count);
            int totalNeurons = hiddenLayerB.Count + hiddenLayerA.Count;
            float neuronExceptionDistance = (totalExcections != 0 && totalNeurons != 0) ? _strategyParams.distanceNeuronMult * (float)(totalExcections / totalNeurons) : 0f;

            //Difference in connections exceptions and weights differences
            //First we intersect the neurons.
            //Then we check connections tables for each neuron
            float avrgWeightDifference = 0f;
            int totalConnExceptions = 0;
            int totalConnections = 0;
            // TODO Add input and output neurons

            //cosine distance
            double coSine_XYSommatory = 0.0f;
            double coSine_Xsommatory = 0.0f;
            double coSine_Ysommatory = 0.0f;

            //jaccard distance
            double threashold = 0.1f;
            int numberOfExceptions = 0;
            int numberOfConnections = 0;

            foreach (var item in CommonNeurons)
            {
                Neuron<T> otherNeuron = networkB.neuronMap[item.UID] as Neuron<T>;
                HashSet<Connection<T>> commonConnections = item.GetConnections();
                commonConnections.IntersectWith(otherNeuron.GetConnections());
                totalConnExceptions += (otherNeuron.GetConnections().Count - commonConnections.Count) + (item.GetConnections().Count - commonConnections.Count);
                totalConnections += (otherNeuron.GetConnections().Count + item.GetConnections().Count);

                float weightDifferencesNeuron = 0f;
                // If we are going to use this for very large network we need to encode a way to store this value every 1000 or so connections.
                // Else we are going to lose differences in the order of 0.001 thanks to floating point weirdness.
                foreach (var connection in commonConnections)
                {
                    float weightA = (connection.Weight as IValue<float>).value;
                    float weightB = (otherNeuron.ConnectionsMap[connection.ID].Weight as IValue<float>).value;
                    weightDifferencesNeuron += (float) Math.Pow (weightA - weightB, 2.0);

                   coSine_XYSommatory += weightA*weightB;
                   coSine_Xsommatory += Math.Pow(weightA, 2.0);
                   coSine_Ysommatory += Math.Pow(weightB, 2.0);
                    if( Math.Abs( weightA - weightB ) > threashold)
                    {
                        numberOfExceptions++;
                    }
                    numberOfConnections++;
                }
                //Console.WriteLine("distance Neuron: " + weightDifferencesNeuron.ToString());

                avrgWeightDifference += (weightDifferencesNeuron / (commonConnections.Count != 0 ? commonConnections.Count : 1));
            }

            coSine_Xsommatory = Math.Sqrt(coSine_Xsommatory);
            coSine_Ysommatory = Math.Sqrt(coSine_Ysommatory);
            coSine_XYSommatory = Math.Acos( coSine_XYSommatory / (coSine_Xsommatory * coSine_Ysommatory) );
            avrgWeightDifference /= (CommonNeurons.Count != 0 ? CommonNeurons.Count : 1);
            Console.WriteLine("Acos: " + (coSine_XYSommatory /** (180.0 / Math.PI)*/).ToString() + " avrgWeight: " + avrgWeightDifference + " jaccard: " + ((float)numberOfExceptions / (float)numberOfConnections).ToString() );
            float connectionExceptionDistance = (totalConnExceptions != 0 && totalConnections != 0) ? _strategyParams.distanceConnectionMult * (totalConnExceptions / totalConnections) : 0f;
            //Console.WriteLine("distance Net: " + (connectionExceptionDistance) + (neuronExceptionDistance) + (avrgWeightDifference * _strategyParams.distanceAvrgWeight).ToString());

            return (connectionExceptionDistance) + (neuronExceptionDistance) + (avrgWeightDifference * _strategyParams.distanceAvrgWeight);
        }

        override public ISolution<T> CrossBreed(ISolution<T> solution, ISolution<T> otherSolution)
        {
            Network<T> NetA = solution.genome as Network<T>;
            Network<T> NetB = otherSolution.genome as Network<T>;

            // we first copy one of the two
            // then we "merge" the differences

            return solution;
        }

    }
}
