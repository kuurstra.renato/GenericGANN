﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Neat.GeneticAlgorithm;
using Neat.GeneticAlgorithm.Strategies;
using Neat.NeuralNetwork;

namespace Neat
{
    class TestBed
    {
        static void Main(string[] args)
        {


            StrategyParams strategyParams = new StrategyParams
            {
                inputNeurons = 1000,
                outputNeurons = 1,
                noiseRate = 1f,
                addNeuron = 0.01f,
                //
                disableNeuron = 0.011f,
                addConnection = 0.02f,
                disableConnection = 0.022f,

                //
                distanceAvrgWeight = 1f,
                distanceConnectionMult = 1f,
                distanceNeuronMult = 1f
            
            };

            NeuronFactory<float> neuronFactory = new NeuronFactory<float>();
            GenomeFactory<float> genomeFactory = new GenomeFactory<float>(strategyParams.inputNeurons, strategyParams.outputNeurons, neuronFactory);

            NeatStrategy<float> strategy = new NeatStrategy<float>(strategyParams, new ConcreteValue(1f), neuronFactory);

            SolutionFactory<float> solutionFactory = new SolutionFactory<float>(strategy, genomeFactory);
            EvolutionaryPool<float> geneticPool = new EvolutionaryPool<float>(15, new EvolutionaryPool<float>.EvolutionParams() { NewSpeciesDistance = 0.4f },solutionFactory);
            do
            {
                while (!Console.KeyAvailable)
                {
                    System.Threading.Thread.Sleep(5000);
                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}
